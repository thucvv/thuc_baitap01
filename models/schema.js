var mongoose = require('mongoose');
var Schema = mongoose.Schema;
mongoose.Promise = global.Promise;
/*User schema-----------------------------------------------------------------------------------------------*/
// create a schema
//Schema la cau truc collection cua db
var userSchema = new Schema({
  name: String,
  email: String,
  password: String,
  created_at: Date,
  updated_at: Date
});

// phương thức này sẽ giúp mình tự động lưu thời gian tạo và thời gian sửa vào thuộc tính created_at và updated_at
userSchema.pre('save', function(next){
  var currentDate = new Date().toISOString();
  this.updated_at = currentDate;
  if(!this.created_at) this.created_at = currentDate;
  next();
});
// the schema is useless so far
// we need to create a model using it
//tao mot collection user 
var User = mongoose.model('User', userSchema);
/*Student schema--------------------------------------------------------------------------------------------*/
var studentSchema = new Schema({
  username: String, 
  email: String, 
  address: String, 
  gender: Boolean
});
var Student = mongoose.model('Student', studentSchema);
// make this available to our users in our Node applications
module.exports = {
  User:User, 
  Student:Student
}