//Route return api 
var express = require('express');
var router = express.Router(); 
var models = require('../models/schema');
var mongoose = require("mongoose");

router.route('/')
.get(function(req, res){
	models.Student.find({}, function(err, students){
		if(err) throw err;
		else{
			models.Student.count({}, function(err, countstd){
				if (err) throw err;
				else{
					res.json({count: countstd, data: students});		
				}
			})
			
		}
	})
});


module.exports = router;